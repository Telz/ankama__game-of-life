'use strict';

import { times } from 'lodash';

import { DEAD, ALIVE } from './constants';

/**
 *  Computes the next board generation and returns a new board.
 *  The board is circular.
 *
 *  @param {Array[]} board - The current board
 *  @returns {Array[]} The board representing the next generation.
 */
export default function nextGeneration(board) {
  // compute each cell individually
  return times(board.length, y => times(board[y].length, (x) => {
    const width = board[y].length;
    const height = board.length;

    // compute x and y based on a circular board
    // x == -1 will become x = width - 1
    // etc.
    const xLeft = ((x - 1) + width) % width;
    const xRight = (x + 1) % width;
    const yAbove = ((y - 1) + height) % height;
    const yBelow = (y + 1) % height;

    // compute the number of alive cells in the 8 cells around
    const adjacentAliveCellsNb = [
      [xLeft, yAbove],
      [xLeft, y],
      [xLeft, yBelow],
      [x, yBelow],
      [xRight, yBelow],
      [xRight, y],
      [xRight, yAbove],
      [x, yAbove],
    ].reduce(
      (count, [xTarget, yTarget]) => {
        // since the board is circular
        // we need to make sure that we are not checking the current cell
        // otherwise a board of 1*1 would live forever
        const isSameCell = (xTarget === x && yTarget === y);

        const isAlive = (board[yTarget][xTarget] === ALIVE);
        return count + (!isSameCell && isAlive ? 1 : 0);
      },
      0,
    );

    // determine if the cells will be alive or dead in the next generation
    switch (board[y][x]) {
      case ALIVE:
        // to stay alive, a cell must have 2 or 3 alive neighbors
        return (adjacentAliveCellsNb === 2 || adjacentAliveCellsNb === 3) ? ALIVE : DEAD;
      case DEAD:
        // to become alive, a cell must have 3 alive neighbors
        return (adjacentAliveCellsNb === 3) ? ALIVE : DEAD;
      default:
        // we should never come here, but if the board was not initialized, set the cell to DEAD
        return DEAD;
    }
  }));
}
