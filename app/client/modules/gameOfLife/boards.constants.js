'use strict';

import { times, random } from 'lodash';

import {
  DEAD,
  ALIVE,
  WIDTH,
  HEIGHT,
  GOSPER_GLIDER_GUN_POSITIONS,
  LINE_BORDER_OFFSET,
} from './constants';

/**
 *  Returns a new board filled with dead cells.
 *
 *  @param {number} [width=WIDTH] - width of the board to create.
 *  @param {number} [height=HEIGHT] - height of the board to create.
 *  @returns {Array[]}
 */
export function EMPTY(width = WIDTH, height = HEIGHT) {
  return times(height, () => times(width, () => DEAD));
}

/**
 *  Returns a new board representing a simple Gosper Glider Gun.
 *
 *  @see {@link http://www.conwaylife.com/w/index.php?title=Gosper_glider_gun}
 *
 *  @param {number} [width=WIDTH] - width of the board to create (must be at least 37).
 *  @param {number} [height=HEIGHT] - height of the board to create (must be at least 10).
 *  @returns {Array[]}
 */
export function GOSPER_GLIDER_GUN(width = WIDTH, height = HEIGHT) {
  const matrix = EMPTY(width, height);

  GOSPER_GLIDER_GUN_POSITIONS.forEach(([x, y]) => {
    matrix[y][x] = ALIVE;
  });

  return matrix;
}

/**
 *  Returns a new board with an horizontal line in the center.
 *
 *  @param {number} [width=WIDTH] - width of the board to create (must be at least 10).
 *  @param {number} [height=HEIGHT] - height of the board to create (must be at least 2).
 *  @returns {Array[]}
 */
export function LINE(width = WIDTH, height = HEIGHT) {
  const matrix = EMPTY(width, height);

  const y1 = Math.floor(height / 2);
  const y2 = y1 - 1;

  matrix[y1].forEach((cell, x) => {
    if (x > LINE_BORDER_OFFSET && x < WIDTH - LINE_BORDER_OFFSET - 1) {
      matrix[y1][x] = ALIVE;
    }
  });

  matrix[y2].forEach((cell, x) => {
    if (x > LINE_BORDER_OFFSET && x < WIDTH - LINE_BORDER_OFFSET - 1) {
      matrix[y2][x] = ALIVE;
    }
  });

  return matrix;
}

/**
 *  Returns a new board filled with a random pattern.
 *  There will be between a quarter and a half of the map filled with alive cells.
 *
 *  @param {number} [width=WIDTH] - width of the board to create (must be positive).
 *  @param {number} [height=HEIGHT] - height of the board to create (must be positive).
 *  @returns {Array[]}
 */
export function RANDOM_BOARD(width = WIDTH, height = HEIGHT) {
  const matrix = EMPTY(width, height);

  // between a quarter and a half, this makes certain that the board will not be overflooded
  const cellsNb = random((width * height) / 4, (width * height) / 2);

  for (let i = 0; i < cellsNb; i += 1) {
    let x;
    let y;

    // make sure to create every cell
    do {
      x = random(0, WIDTH - 1);
      y = random(0, HEIGHT - 1);
    } while (matrix[y][x] !== DEAD);

    matrix[y][x] = ALIVE;
  }

  return matrix;
}

/** Boards options in drop down list */
export const BOARDS = {
  glider: { label: 'Gosper Glider Gun', create: GOSPER_GLIDER_GUN },
  line: { label: 'Ligne', create: LINE },
  empty: { label: 'Vide', create: EMPTY },
  random: { label: 'Aléatoire', create: RANDOM_BOARD },
};
