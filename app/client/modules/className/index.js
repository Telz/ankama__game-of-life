'use strict';

import { isPlainObject } from 'lodash';

/**
 *  Concatenates css class names and returns a string.
 *
 *  @example
 *  // returns 'foo bar foo--active'
 *  concat(
 *    'foo',
 *    'bar',
 *    { className: 'foo--active', isActive: true },
 *    { className: 'bar--active', isActive: false }
 *  );
 *
 *  @param {...(string|Object)} classNames - arbitrary number of class names to concat.
 *  @param {string} classNames.className - label of the class if the argument is an Object.
 *  @param {boolean} classNames.isActive - whether or not to add the class name to the list.
 *
 *  @returns {string}
 */
export default function concat(...classNames) {
  return classNames
    .map((className) => {
      if (isPlainObject(className)) {
        return className.isActive ? className.className : '';
      }

      return className;
    })
    .join(' ')
    .trim();
}
