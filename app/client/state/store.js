'use strict';

import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';

import reducers from './reducers';
import sagas from './sagas';

/**
 *  @module
 *  @description This module exports the redux store of the app
 */

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware];

// in development, add a logger for each task
// remove it in production because it destroys the performance of the game
if (process.env.NODE_ENV !== 'production') {
  // the logger must always be the last middleware
  middlewares.push(createLogger({
    collapsed: true,
    duration: true,
  }));
}

// create the store from our reducers
const store = createStore(
  reducers,
  applyMiddleware(...middlewares),
);

// start each saga after creating the store
sagas.forEach(saga => sagaMiddleware.run(saga));

export default store;
