'use strict';

/**
 *  @module
 *  @description These are the redux actions for the game.
 *  Each method returns a redux action describing something that happened in the app.
 */

/** Init/reinit the game */
export const init = () => ({ type: 'GAME/INIT' });

/** The board is ready, start or resume the game */
export const start = () => ({ type: 'GAME/START' });

/** Pause the game */
export const pause = () => ({ type: 'GAME/PAUSE' });

/** Process the next generation */
export const nextGeneration = () => ({ type: 'GAME/NEXT_GENERATION' });

/** Start the camera */
export const startCamera = () => ({ type: 'GAME/START_CAMERA' });

/** Stop the camera */
export const stopCamera = () => ({ type: 'GAME/STOP_CAMERA' });

/** Disable the camera control */
export const disableCamera = () => ({ type: 'GAME/DISABLE_CAMERA' });

/**
 *  Set the new state of the game
 *  @param {Array[]} board - the new board
 */
export const setBoard = board => ({
  type: 'GAME/SET_BOARD',
  payload: {
    board,
  },
});

/**
 *  Set the new speed of the game
 *  @param {number} speed - the new speed, as a delay between each generation in ms.
 */
export const setSpeed = speed => ({
  type: 'GAME/SET_SPEED',
  payload: {
    speed,
  },
});

/**
 *  Open or close the board selection control
 *  @param {bool} isOpen
 */
export const setBoardSelectionOpen = isOpen => ({
  type: 'GAME/SET_BOARD_SELECTION_OPEN',
  payload: {
    isOpen,
  },
});

/**
 *  Keep the latest selected board in board selection control
 *  @param {string} board
 */
export const selectBoard = board => ({
  type: 'GAME/SELECT_BOARD',
  payload: {
    board,
  },
});

/**
 *  Change the camera status after successfully starting/stopping it.
 *  @param {bool} isCapturing - True if the camera is on, false if the camera is off.
 */
export const setCameraStatus = isCapturing => ({
  type: 'GAME/SET_CAMERA_STATUS',
  payload: {
    isCapturing,
  },
});
