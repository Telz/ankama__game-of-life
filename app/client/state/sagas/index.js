'use strict';

import game from './game.saga';
import camera from './camera.saga';

/**
 *  This file exports every saga of the app.
 */

export default [
  game,
  camera,
];
