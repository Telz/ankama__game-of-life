'use strict';

import { delay } from 'redux-saga';
import { take, put, select, race, call } from 'redux-saga/effects';

import { nextGeneration } from '../actions/game.actions';

/**
 *  This saga represents the flow of the game.
 *  We wait for a GAME/START action,
 *  then wait for the delay of the next tick or an action that pauses the game.
 *
 *  GAME/PAUSE, GAME/INIT and GAME/SET_BOARD restart the flow.
 *
 *  If the delay is passed and none of these actions have been dispatched,
 *  we process the next generation and wait for the next tick delay to be over
 */
export default function* gameFlow() {
  while (true) { // eslint-disable-line no-constant-condition
    // wait for GAME/START action to be dispatched
    yield take('GAME/START');

    while (true) { // eslint-disable-line no-constant-condition
      // get the speed in fps
      const speed = yield select(state => state.game.speed);

      // wait until any of the following events happens
      const { pause, init, setBoard, startCamera } = yield race({
        // convert fps to a ms delay
        // then wait for this delay
        tick: call(delay, 1000 / speed),

        // wait for these actions
        pause: take('GAME/PAUSE'),
        init: take('GAME/INIT'),
        setBoard: take('GAME/SET_BOARD'),
        startCamera: take('GAME/START_CAMERA'),
      });

      // if any of the event was caught before the delay was up
      // wait for GAME/START again
      if (pause || setBoard || init || startCamera) {
        break;
      }

      // if the delay was the first to return, process next generation
      yield put(nextGeneration());
    }
  }
}
