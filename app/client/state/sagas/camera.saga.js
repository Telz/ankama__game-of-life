'use strict';

import { times } from 'lodash';
import { delay } from 'redux-saga';
import { take, put, race, call } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import Camera from 'lfcamera';

import {
  pause,
  setBoard,
  disableCamera,
  setCameraStatus,
} from '../actions/game.actions';

import { WIDTH, HEIGHT, ALIVE, DEAD } from '../../modules/gameOfLife/constants';

const CAMERA_FPS = 10;

const camera = new Camera({
  /** each pixel will be a cell of our board */
  width: WIDTH,
  height: HEIGHT,

  // mirroring the camera is more intuitive
  mirror: true,
});

/**
 *  This saga represents the flow of the camera.
 */
export default function* cameraFlow() {
  while (true) { // eslint-disable-line no-constant-condition
    yield take('GAME/START_CAMERA');

    // pause the game when starting the camera
    yield put(pause());

    try {
      // start video feed
      yield camera.init();
    } catch (err) {
      // if no camera
      // disable the camera control
      yield put(disableCamera());

      // display error message
      yield toast.error('Votre caméra semble être désactivée');
      break;
    }

    // if the camera inited successfully
    yield put(setCameraStatus(true));

    while (true) { // eslint-disable-line no-constant-condition
      // wait until any of the events occurs
      const {
        onStopCamera,
        onStart,
        onPause,
        onInit,
        onNextGeneration,
      } = yield race({
        // wait for end of tick
        tick: call(delay, 1000 / CAMERA_FPS),

        // wait for these actions
        onStopCamera: take('GAME/STOP_CAMERA'),
        onNextGeneration: take('GAME/NEXT_GENERATION'),
        onStart: take('GAME/START'),
        onPause: take('GAME/PAUSE'),
        onInit: take('GAME/INIT'),
      });

      // if any of the events occurred before the delay was up
      // stop the camera
      if (onStopCamera || onNextGeneration || onStart || onPause || onInit) {
        yield camera.stop();
        yield put(setCameraStatus(false));
        break;
      }

      // if the delay was the first to return, process camera frame
      const canvas = camera.getFrame();
      const context = canvas.getContext('2d');
      const imageData = context.getImageData(0, 0, canvas.width, canvas.height);

      // create a new board by computing each "pixel"
      const board = times(canvas.height, y => times(canvas.width, (x) => {
        // each pixel is divided in 4 bytes (RGBA)
        const offset = ((y * canvas.width) + x) * 4;
        const color = {
          red: imageData.data[offset],
          green: imageData.data[offset + 1],
          blue: imageData.data[offset + 2],
          alpha: imageData.data[offset + 3],
        };

        // get pixel brightness (got formula from https://www.w3.org/TR/AERT#color-contrast)
        // between 0 and 1
        const brightness = (
          ((0.299 * color.red) +
            (0.587 * color.green) +
            (0.114 * color.blue)) / 255);

        // bright is dead (not sure why)
        return (brightness > 0.5 ? DEAD : ALIVE);
      }));

      yield put(setBoard(board));
    }
  }
}
