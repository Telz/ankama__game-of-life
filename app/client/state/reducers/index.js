'use strict';

import { combineReducers } from 'redux';

import gameReducers from './game.reducers';

/**
 *  This module exports every reducers of the app.
 */

export default combineReducers({
  game: gameReducers,
});
