'use strict';

import { combineReducers } from 'redux';

import nextGeneration from '../../modules/gameOfLife/game';
import { EMPTY } from '../../modules/gameOfLife/boards.constants';

/**
 *  @module
 *  @description These are the reducers of the game
 *  Representing the current state of the game
 */

/** Reducer for the state.game.isRunning state */
const isRunning = (state = false, action) => {
  switch (action.type) {
    // these actions start the game
    case 'GAME/START':
      return true;

    // these actions stop the game
    case 'GAME/INIT':
    case 'GAME/SET_BOARD':
    case 'GAME/START_CAMERA':
    case 'GAME/PAUSE':
      return false;

    // unhandled action, return current state
    default:
      return state;
  }
};

/** Reducer for the state.game.isRunning state, default is an empty board */
const board = (state = EMPTY(), action) => {
  switch (action.type) {
    // GAME/INIT resets the board to an empty board
    case 'GAME/INIT':
      return EMPTY();

    // GAME/SET_BOARD sets the new value of the board
    case 'GAME/SET_BOARD':
      return action.payload.board;

    // GAME/NEXT_GENERATION sets the new value of the board as the next generation
    case 'GAME/NEXT_GENERATION':
      return nextGeneration(state);

    // unhandled action, return current state
    default:
      return state;
  }
};

/** speed of the game in fps */
const speed = (state = 20, action) => {
  switch (action.type) {
    // GAME/SET_SPEED sets the speed in fps
    case 'GAME/SET_SPEED':
      return action.payload.speed;

    // unhandled action, return current state
    default:
      return state;
  }
};

/** Generations count of the current game */
const generationCount = (state = 0, action) => {
  switch (action.type) {
    // GAME/INIT reinits the counter
    case 'GAME/INIT':
    case 'GAME/START_CAMERA':
      return 0;

    // GAME/NEXT_GENERATION increments the counter
    case 'GAME/NEXT_GENERATION':
      return state + 1;

    // unhandled action, return current state
    default:
      return state;
  }
};

/** Whether or not the camera is on */
const isVideoCapturing = (state = false, action) => {
  switch (action.type) {
    case 'GAME/SET_CAMERA_STATUS':
      return action.payload.isCapturing;

    // unhandled action, return current state
    default:
      return state;
  }
};

/** Whether or not the board selection control is open */
const isBoardSelectionOpen = (state = false, action) => {
  switch (action.type) {
    case 'GAME/SET_BOARD_SELECTION_OPEN':
      return action.payload.isOpen;

    // close the selection when the game is starting
    case 'GAME/START':
      return false;

    // unhandled action, return current state
    default:
      return state;
  }
};

/** Last selected board in board selection control */
const selectedBoard = (state = 'random', action) => {
  switch (action.type) {
    case 'GAME/SELECT_BOARD':
      return action.payload.board;

    // unhandled action, return current state
    default:
      return state;
  }
};

const isCameraDisabled = (state = false, action) => {
  switch (action.type) {
    case 'GAME/DISABLE_CAMERA':
      return true;

    // unhandled action, return current state
    default:
      return state;
  }
};

// export reducers using combineReducers
// so we only have the state of the reducer inside each function
// instead of the entire state tree
export default combineReducers({
  isRunning,
  board,
  speed,
  generationCount,
  isVideoCapturing,
  isBoardSelectionOpen,
  selectedBoard,
  isCameraDisabled,
});
