'use strict';

import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import App from './react/views/App';
import store from './state/store';

import './style/main.scss';

// The app mount point is the #app element of index.html
const mountPoint = document.getElementById('app');

// Mount our app
ReactDOM.render(
  // Add the redux store to the context using the Provider component
  <Provider store={store}>
    <div>
      <App />
      <ToastContainer />
    </div>
  </Provider>,
  mountPoint,
);
