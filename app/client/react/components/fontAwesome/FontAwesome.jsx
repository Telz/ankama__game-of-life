'use strict';

import React from 'react';
import PropTypes from 'prop-types';

import concat from '../../../modules/className';
import './fontawesome.scss';

/**
 *  font-awesome component.
 *
 *  @example
 *  <FontAwesome name="play" />
 */
export default class FontAwesome extends React.PureComponent {
  /** Expected props */
  static get propTypes() {
    return {
      /** name of the icon in fontawesome (without the fa-) */
      name: PropTypes.string.isRequired,
    };
  }

  /** Renders the icon */
  render() {
    const {
      name,
      ...props
    } = this.props;

    props.className = concat(
      props.className,
      'fa',
      `fa-${name}`,
    );

    return (
      <i {...props} />
    );
  }
}
