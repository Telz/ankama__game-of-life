'use strict';

import React from 'react';
import PropTypes from 'prop-types';

import classNames from './layout.module.scss';

/**
 *  Layout of the app. Provides a flex context.
 */
export default class Layout extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      children: PropTypes.node,
    };
  }

  /** Default props values. */
  static get defaultProps() {
    return {
      children: null,
    };
  }

  /** Renders the layout wrapping the children */
  render() {
    return (
      <div className={classNames.layout}>
        <main className={classNames.content}>
          {this.props.children}
        </main>
      </div>
    );
  }
}
