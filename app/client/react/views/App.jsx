'use strict';

import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Board from '../app/board/containers/Board';
import Controls from '../app/controls/containers/Controls';
import Layout from '../components/layout/Layout';

import { init, setBoard } from '../../state/actions/game.actions';
import { RANDOM_BOARD } from '../../modules/gameOfLife/boards.constants';

/**
 *  Main (and only) view.
 */
@connect()
export default class AppView extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      /** From @connect */
      dispatch: PropTypes.func.isRequired,
    };
  }

  /** Display a random board when starting the app */
  componentWillMount() {
    this.props.dispatch(init());
    this.props.dispatch(setBoard(RANDOM_BOARD()));
  }

  /** Display the board and controls */
  render() {
    return (
      <Layout>
        <Board />
        <Controls />
      </Layout>
    );
  }
}
