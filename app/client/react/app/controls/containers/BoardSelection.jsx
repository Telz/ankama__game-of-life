'use strict';

import React from 'react';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';
import PropTypes from 'prop-types';

import BoardSelectionPresentation from '../presentation/BoardSelection';

import { init, setBoard, setBoardSelectionOpen, selectBoard } from '../../../../state/actions/game.actions';

import { BOARDS } from '../../../../modules/gameOfLife/boards.constants';

/**
 *  @class
 *  @description Container component of the board selection control.
 */
@connect(state => ({
  isBoardSelectionOpen: state.game.isBoardSelectionOpen,
  selectedBoard: state.game.selectedBoard,
}))
export default class BoardSelectionContainer extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      /** From @connect */
      dispatch: PropTypes.func.isRequired,
      /** From Redux store in @connect */
      isBoardSelectionOpen: PropTypes.bool.isRequired,
      /** From Redux store in @connect */
      selectedBoard: PropTypes.string.isRequired,
    };
  }

  /**
   *  Given a value from the BOARDS object, creates a new board and reinits the game.
   *  Stops the camera.
   *  @param {string} value - property in BOARDS object.
   */
  @autobind
  changeBoard(value) {
    const board = BOARDS[value].create();

    this.props.dispatch(init());
    this.props.dispatch(setBoard(board));
    this.props.dispatch(selectBoard(value));
  }

  /**
   *  Open or close the board selection control
   */
  @autobind
  toggleBoardSelectionOpen() {
    this.props.dispatch(setBoardSelectionOpen(!this.props.isBoardSelectionOpen));
  }

  /** Renders the board selection presentation */
  render() {
    const {
      selectedBoard,
      isBoardSelectionOpen,
    } = this.props;

    return (
      <BoardSelectionPresentation
        selectedBoard={selectedBoard}
        isBoardSelectionOpen={isBoardSelectionOpen}
        toggleBoardSelectionOpen={this.toggleBoardSelectionOpen}
        boards={BOARDS}
        changeBoard={this.changeBoard}
      />
    );
  }
}
