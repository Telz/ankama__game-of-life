'use strict';

import React from 'react';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';
import PropTypes from 'prop-types';

import SpeedPresentation from '../presentation/Speed';

import { setSpeed } from '../../../../state/actions/game.actions';

/**
 *  @class
 *  @description Container component of the speed control.
 *  Bound to the store and listening to changes on the speed.
 */
@connect(state => ({
  speed: state.game.speed,
}))
export default class SpeedContainer extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      /** From Redux store in @connect */
      speed: PropTypes.number.isRequired,
      /** From @connect */
      dispatch: PropTypes.func.isRequired,
    };
  }

  /**
   *  Sets the speed parameter for the game.
   *  @param {number} speed - number of fps wished
   */
  @autobind
  setSpeed(speed) {
    this.props.dispatch(setSpeed(speed));
  }

  /** Renders the speed control presentation */
  render() {
    const { speed } = this.props;

    return (
      <SpeedPresentation
        speed={speed}
        setSpeed={this.setSpeed}
      />
    );
  }
}
