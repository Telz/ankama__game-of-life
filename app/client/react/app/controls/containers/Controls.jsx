'use strict';

import React from 'react';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';
import PropTypes from 'prop-types';

import ControlsPresentation from '../presentation/Controls';

import { init, start, pause, nextGeneration, setBoard } from '../../../../state/actions/game.actions';

import { BOARDS } from '../../../../modules/gameOfLife/boards.constants';

/**
 *  @class
 *  @description Container component of the controls.
 *  Bound to the store and listening to changes on the play state and number of generations.
 */
@connect(state => ({
  isRunning: state.game.isRunning,
  generationCount: state.game.generationCount,
  selectedBoard: state.game.selectedBoard,
}))
export default class ControlsContainer extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      /** From @connect */
      dispatch: PropTypes.func.isRequired,
      /** From Redux store in @connect */
      isRunning: PropTypes.bool.isRequired,
      /** From Redux store in @connect */
      generationCount: PropTypes.number.isRequired,
      /** From redux store in @connect */
      selectedBoard: PropTypes.string.isRequired,
    };
  }

  /**
   *  Stops the camera and starts the game
   */
  @autobind
  start() {
    return this.props.dispatch(start());
  }

  /**
   *  Pauses the game
   */
  @autobind
  pause() {
    return this.props.dispatch(pause());
  }

  /**
   *  Processes the next generation
   *  Stops the camera
   */
  @autobind
  nextGeneration() {
    this.pause();
    return this.props.dispatch(nextGeneration());
  }

  /**
   *  Reinits the game with the same board type as last selected.
   */
  @autobind
  reinit() {
    const board = BOARDS[this.props.selectedBoard].create();
    this.props.dispatch(init());
    this.props.dispatch(setBoard(board));
  }

  /** Renders the controls presentation */
  render() {
    const {
      isRunning,
      generationCount,
    } = this.props;

    return (
      <ControlsPresentation
        isRunning={isRunning}
        generationCount={generationCount}
        start={this.start}
        pause={this.pause}
        reinit={this.reinit}
        nextGeneration={this.nextGeneration}
      />
    );
  }
}
