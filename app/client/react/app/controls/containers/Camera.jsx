'use strict';

import React from 'react';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';
import PropTypes from 'prop-types';

import CameraPresentation from '../presentation/Camera';

import { startCamera, stopCamera } from '../../../../state/actions/game.actions';

/**
 *  @class
 *  @description This container class manages the camera control and video feed.
 */
@connect(state => ({
  isVideoCapturing: state.game.isVideoCapturing,
  isCameraDisabled: state.game.isCameraDisabled,
}))
export default class ControlsContainer extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      /** From @connect */
      dispatch: PropTypes.func.isRequired,
      /** From Redux store in @connect */
      isVideoCapturing: PropTypes.bool.isRequired,
      /** From Redux store in @connect */
      isCameraDisabled: PropTypes.bool.isRequired,
    };
  }

  /**
   *  When the component unmounts, stop the camera if necessary.
   */
  componentWillUnmount() {
    this.props.dispatch(stopCamera());
  }

  /**
   *  Stops the video feed and board update.
   */
  @autobind
  stopVideoCapture() {
    this.props.dispatch(stopCamera());
  }

  /**
   *  Toggles the video feed.
   */
  @autobind
  toggleVideoCapture() {
    const wasCapturing = this.props.isVideoCapturing;

    if (wasCapturing) {
      // if the camera is currently on, stops it
      this.props.dispatch(stopCamera());
    } else {
      // if the camera is currently off, starts it
      this.props.dispatch(startCamera());
    }
  }

  /** Renders the controls presentation */
  render() {
    const {
      isVideoCapturing,
      isCameraDisabled,
    } = this.props;

    return (
      <CameraPresentation
        isVideoCapturing={isVideoCapturing}
        isCameraDisabled={isCameraDisabled}
        toggleVideoCapture={this.toggleVideoCapture}
      />
    );
  }
}
