'use strict';

import React from 'react';
import PropTypes from 'prop-types';

import FontAwesome from '../../../components/fontAwesome/FontAwesome';

import concat from '../../../../modules/className';
import classNames from './button.module.scss';

/**
 *  Presentational component for a control button
 */
export default class ControlsPresentation extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      /** Name of font awesome icon */
      name: PropTypes.string.isRequired,
      /** Action when clicking on button */
      onClick: PropTypes.func.isRequired,
      /** Whether or not the control is in its active state */
      isActive: PropTypes.bool,
      /** Whether or not the control is in its disabled state */
      isDisabled: PropTypes.bool,
    };
  }

  /** Default props values. */
  static get defaultProps() {
    return {
      isActive: false,
      isDisabled: false,
    };
  }

  /** Render the controls */
  render() {
    const {
      isActive,
      isDisabled,
    } = this.props;

    const className = concat(
      classNames.button,
      { className: classNames['button--active'], isActive: !isDisabled && isActive },
      { className: classNames['button--disabled'], isActive: isDisabled },
    );

    return (
      <button className={className}>
        <FontAwesome
          name={this.props.name}
          onClick={isDisabled ? undefined : this.props.onClick}
        />
      </button>
    );
  }
}
