'use strict';

import React from 'react';
import PropTypes from 'prop-types';

import Button from './Button';
import Speed from '../containers/Speed';
import Camera from '../containers/Camera';
import BoardSelection from '../containers/BoardSelection';

import classNames from './controls.module.scss';

/**
 *  Presentational component for the controls
 */
export default class ControlsPresentation extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      /** Whether or not the game is running */
      isRunning: PropTypes.bool.isRequired,
      /** Function to call to start the game */
      start: PropTypes.func.isRequired,
      /** Function to call to pause the game */
      pause: PropTypes.func.isRequired,
      /** Function to call to process the next generation */
      nextGeneration: PropTypes.func.isRequired,
      /** Function to call to reinit the game */
      reinit: PropTypes.func.isRequired,
      /** Number of generations since the start of the game */
      generationCount: PropTypes.number.isRequired,
    };
  }

  /** Render the controls */
  render() {
    return (
      <div className={classNames.container}>
        <div className={classNames['actions--left']}>

          { /** Play/pause control */ }
          { !this.props.isRunning ?
            <Button
              name={'play'}
              onClick={this.props.start}
            />
            : <Button
              name={'pause'}
              onClick={this.props.pause}
            />
          }

          { /** Next generation control */}
          <Button
            name={'chevron-right'}
            onClick={this.props.nextGeneration}
          />

          { /** generation count */ }
          <div className={classNames['generations-container']}>
            Génération&nbsp;
            <span className={classNames.generations__value}>{this.props.generationCount}</span>
          </div>
        </div>

        <div className={classNames['actions--right']}>
          { /** Speed selection control */ }
          <Speed />

          { /** Grid control */ }
          <BoardSelection />

          { /** Camera control */ }
          <Camera />

          { /** Reinit control */ }
          <Button
            name={'refresh'}
            onClick={this.props.reinit}
          />
        </div>
      </div>
    );
  }
}
