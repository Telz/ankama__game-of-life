'use strict';

import React from 'react';
import PropTypes from 'prop-types';

import Button from './Button';

/**
 *  Presentational component for the camera control
 */
export default class CameraPresentation extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      /** Callback to call to toggle video capture */
      toggleVideoCapture: PropTypes.func.isRequired,
      /** Whether or not the camera is active */
      isVideoCapturing: PropTypes.bool.isRequired,
      /** Whether or not the camera is enabled (false if failed to start once) */
      isCameraDisabled: PropTypes.bool.isRequired,
    };
  }

  /** Render the controls */
  render() {
    return (
      <Button
        name={'camera'}
        isActive={this.props.isVideoCapturing}
        isDisabled={this.props.isCameraDisabled}
        onClick={this.props.toggleVideoCapture}
      />
    );
  }
}
