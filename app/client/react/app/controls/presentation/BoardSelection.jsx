'use strict';

import { map } from 'lodash';
import React from 'react';
import autobind from 'autobind-decorator';
import PropTypes from 'prop-types';

import Button from './Button';

import classNames from './boardselection.module.scss';

/**
 *  Renders an option of board configuration
 */
class BoardOption extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      /** the value returned on click */
      value: PropTypes.any.isRequired,
      /** the value displayed on screen */
      label: PropTypes.node.isRequired,
      /** the function called with the value */
      onClick: PropTypes.func.isRequired,
    };
  }

  /**
   *  Calls the parent onClick callback with the component value prop.
   */
  @autobind
  onClick() {
    this.props.onClick(this.props.value);
  }

  /** Renders the option */
  render() {
    return (
      <button
        className={classNames['board-option']}
        onClick={this.onClick}
      >
        {this.props.label}
      </button>
    );
  }
}

/**
 *  Presentational component for the controls
 */
export default class ControlsPresentation extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      /** the boards options */
      boards: PropTypes.object.isRequired,
      /** Display the board selection control or not */
      isBoardSelectionOpen: PropTypes.bool.isRequired,
      /** Function to call to change the board using the board options */
      changeBoard: PropTypes.func.isRequired,
      /** Function to call to open or close the board selections control */
      toggleBoardSelectionOpen: PropTypes.func.isRequired,
    };
  }

  /** Render the controls */
  render() {
    return (
      <div>
        { /** Grid control */ }
        <Button
          name={'th'}
          onClick={this.props.toggleBoardSelectionOpen}
          isActive={this.isBoardSelectionOpen}
        />

        { /** Only render the board selection control when it is open */ }
        { this.props.isBoardSelectionOpen ?
          <div className={classNames['board-select']}>
            { map(this.props.boards, (board, value) => (
              <BoardOption
                key={value}
                value={value}
                label={board.label}
                onClick={this.props.changeBoard}
              />
            )) }
          </div>
          : null }

      </div>
    );
  }
}
