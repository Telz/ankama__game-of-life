'use strict';

import React from 'react';
import PropTypes from 'prop-types';

import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

import FontAwesome from '../../../components/fontAwesome/FontAwesome';

import classNames from './speed.module.scss';
import colors from '../../../../style/colors.variables.scss';

/** keep styles as constants so their references don't change */
const styles = {
  track: [{
    background: colors.blue,
  }],

  handle: [{
    border: '2px solid white',
    background: colors.blue,
  }],
};

/**
 *  Presentational component for the speed control
 */
export default class SpeedPresentation extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      /** speed of the game in fps */
      speed: PropTypes.number.isRequired,
      /** function to call to change the speed of the game */
      setSpeed: PropTypes.func.isRequired,
    };
  }

  render() {
    return (
      <div className={classNames['slider-container']}>
        <FontAwesome
          name={'internet-explorer'}
          className={classNames.slider__icon}
        />

        <Slider
          // between 1 and 60 fps
          min={1}
          max={60}

          // set as default value to prevent visual glitches
          // let the slider handle its internal value
          defaultValue={this.props.speed}
          onChange={this.props.setSpeed}

          // override styles
          trackStyle={styles.track}
          handleStyle={styles.handle}
        />

        <FontAwesome
          name={'chrome'}
          className={classNames.slider__icon}
        />
      </div>
    );
  }
}
