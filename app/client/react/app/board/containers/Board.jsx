'use strict';

import { cloneDeep } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';

import BoardPresentation from '../presentation/Board';

import { ALIVE, DEAD } from '../../../../modules/gameOfLife/constants';
import { setBoard } from '../../../../state/actions/game.actions';

/**
 *  @class
 *  @description Container component of a board.
 *  Bound to the store and listening to changes on the board.
 */
@connect(state => ({
  board: state.game.board,
}))
export default class BoardContainer extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      /** From Redux store in @connect */
      board: PropTypes.array,
      /** From @connect */
      dispatch: PropTypes.func.isRequired,
    };
  }

  /** Default props values. */
  static get defaultProps() {
    return {
      board: [],
    };
  }

  /**
   *  @method
   *  @description When the user clicks on a cell, this will toggle its state (ALIVE <-> DEAD)
   *
   *  @param {number} x - X coordinate of the cell in the board (horizontal)
   *  @param {number} y - Y coordinate of the cell in the board (vertical)
   */
  @autobind
  onClickCell(x, y) {
    // clone the board to get a new reference (redux state is immutable)
    const board = cloneDeep(this.props.board);

    // toggle the cell state
    board[y][x] = this.props.board[y][x] === ALIVE ? DEAD : ALIVE;

    // replace the board
    this.props.dispatch(setBoard(board));
  }

  /** Render the Board presentation */
  render() {
    const { board } = this.props;

    return (
      <BoardPresentation
        board={board}
        onClickCell={this.onClickCell}
      />
    );
  }
}
