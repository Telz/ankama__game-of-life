'use strict';

import React from 'react';
import autobind from 'autobind-decorator';
import PropTypes from 'prop-types';

import { ALIVE, HEIGHT, WIDTH } from '../../../../modules/gameOfLife/constants';

import colors from '../../../../style/colors.variables.scss';
import classNames from './board.module.scss';

/** Size of a cell in pixels */
const CELL_SIZE = 10;
/** Size of the grid's borders in pixels */
const BORDER_SIZE = 1;

/**
 *  Presentational component of the Board.
 *  Receives a board in its props and renders it.
 */
export default class BoardPresentation extends React.PureComponent {
  /** Expected props. */
  static get propTypes() {
    return {
      board: PropTypes.array,
      onClickCell: PropTypes.func,
    };
  }

  /** Default props values. */
  static get defaultProps() {
    return {
      board: [],
      onClickCell: () => {},
    };
  }

  /** On first mount, draw the board */
  componentDidMount() {
    this.drawBoard();
  }

  /**
   *  On board updates, re-draw the board.
   *
   *  @param {Object} prevProps - value of the props in the previous React cycle
   */
  componentDidUpdate(prevProps) {
    this.drawBoard(prevProps);
  }

  /**
   *  When clicking on the canvas, give the container component the coordinates of the cell.
   */
  @autobind
  onClick(event) {
    // infer the coordinates from the click position
    // no need to offset page scroll since the canvas is at 0/0
    const x = Math.floor(event.pageX / CELL_SIZE);
    const y = Math.floor(event.pageY / CELL_SIZE);

    this.props.onClickCell(x, y);
  }

  /**
   *  Keep a reference to the canvas element when mounted.
   *  @param {node} el.
   */
  @autobind
  setCanvasRef(el) {
    this.canvas = el;
  }

  /**
   *  Draw the board inside a <canvas> element
   *  Using classic React re-rendering has extremely poor performances (< 5 fps)
   *  Probably due to the Redux architecture which makes us duplicate the board each generation
   *
   *  canvas rendering allows us to go to about 50 fps easily.
   */
  drawBoard(prevProps = {}) {
    const { board } = prevProps;

    const ctx = this.canvas.getContext('2d');

    // only draw cells which changed state since last render
    // and draw all cells at first render
    this.props.board.forEach((line, y) => {
      line.forEach((cell, x) => {
        if (!board || this.props.board[y][x] !== board[y][x]) {
          // alive cells are blue, dead cells are grey
          ctx.fillStyle = cell === ALIVE ? colors.blue : '#eaeaea';

          // compute coordinates and draw
          ctx.fillRect(
            CELL_SIZE * x, // X
            CELL_SIZE * y, // Y
            CELL_SIZE - BORDER_SIZE, // width
            CELL_SIZE - BORDER_SIZE, // height
          );
        }
      });
    });
  }

  /** render the canvas element */
  render() {
    return (
      <canvas
        className={classNames.board}
        ref={this.setCanvasRef}
        width={WIDTH * (CELL_SIZE)}
        height={HEIGHT * (CELL_SIZE)}
        onClick={this.onClick}
      />
    );
  }
}
