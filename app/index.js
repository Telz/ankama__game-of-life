'use strict';

const path = require('path');
const url = require('url');

const { app, BrowserWindow } = require('electron');

// from https://electron.atom.io/docs/tutorial/quick-start/

// keep a global ref to the window to prevent garbage collection
let win;

/**
 *  Create the electron app window
 */
function createWindow() {
  win = new BrowserWindow({
    // dimensions are fixed
    width: 807,
    height: 480,
    resizable: false,
    maximizable: false,

    // hide menu bar for esthetics
    autoHideMenuBar: true,
  });

  // start the app using our client/index.html file
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'client', 'index.html'),
    protocol: 'file:',
    slashes: true,
  }));

  // open dev tools in development
  if (process.env.NODE_ENV !== 'production') {
    win.webContents.openDevTools();
  }

  // let the window object be garbage collected when the window is closed
  win.on('closed', () => {
    win = null;
  });
}

// after electron has finished initializing, render the window
app.on('ready', createWindow);

// on OSX, it is common for an app to stay active until the user quits explicitly
app
  .on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit();
    }
  })
  .on('activate', () => {
    // recreate window if the user clicked on close and did not quit the app
    if (win === null) {
      createWindow();
    }
  });
