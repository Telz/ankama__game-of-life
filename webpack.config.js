'use strict';

const path = require('path');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');
const { endsWith, merge } = require('lodash');

// extract css into file
// path is relative to the js bundle folder
const extractCss = new ExtractTextPlugin({
  filename: 'css/style.min.css'
});

const loaders = {
  // .js and .jsx files are loaded with babel-loader
  js: {
    test: /\.jsx?$/i,
    exclude: /(node_modules)/,
    loader: 'babel-loader',
    // babel configuration is in .babelrc (needed by Jest)
  },

  // .css files are loaded with css-loader
  css: (isDev) => ({
    test: /\.css$/i,
    use: extractCss.extract({
      publicPath: '/',
      use: [
        {
          loader: 'css-loader',
          options: {
            // disable css-modules
            modules: false,
            // only minimize in production build
            minimize: !isDev,
            // create a sourcemap in development build
            sourceMap: !!isDev
          }
        }
      ]
    })
  }),

  // simple .scss files are loaded with sass-loader and css-loader
  sass: (isDev) => ({
    // only match .scss files, not .module.scss not .variables.scss (handled separately)
    test: input => endsWith(input, '.scss') && !endsWith(input, '.module.scss') && !endsWith(input, '.variables.scss'),
    use: extractCss.extract({
      publicPath: '/',
      use: [
        {
          loader: 'css-loader',
          options: {
            // disable css-modules
            modules: false,
            // only minimize in production build
            minimize: !isDev,
            // create a sourcemap in development build
            sourceMap: !!isDev
          }
        },
        {
          // this loader resolves url that sass-loader skipped
          // since sass-loader does not resolve url, webpack fails to find the required files
          loader: 'resolve-url-loader'
        },
        {
          // sass to css
          loader: 'sass-loader',
          options: {
            // resolve-url-loader requires previous loaders to set sourceMap to true
            sourceMap: true,
            // resolve @import from css files
            includePaths: [
              './app/client/',
              './node_modules/'
            ]
          }
        }
      ]
    })
  }),

  // sass modules files are loaded the same way as classic .scss files, but with css-modules enabled
  sassModule: (isDev) => ({
    test: /\.module\.scss$/i,
    use: extractCss.extract({
      publicPath: '/',
      use: [
        {
          loader: 'css-loader',
          options: {
            // enable css-modules to avoid conflicts between class names
            modules: true,
            minimize: !isDev,
            sourceMap: true,
            localIdentName: isDev ? '[local]--[hash:base64:5]' : undefined
          }
        },
        {
          loader: 'resolve-url-loader'
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            // resolve @import from css files
            includePaths: [
              './app/client/',
              './node_modules/'
            ]
          }
        }
      ]
    })
  }),

  // allow importing of .variables.scss files from js
  variablesSass: {
    test: /\.variables\.scss$/i,
    loader: 'sass-variable-loader'
  },

  // import resource files and put them in assets folder
  url: {
    test: /\.(png|jpg|ico|gif|svg|eot|eot|otf|ttf|woff|woff2)$/i,
    loader: 'url-loader',
    options: {
      // below this limit (in bytes), the loader returns a base64 representation of the resource
      // above, it copies the file to the folder specified below
      limit: 10000,

      // replace root path in requires to this string
      publicPath: '../../assets/',
      // output path is relative to the JS bundle folder
      outputPath: 'packed/'
    }
  }
};

// shared configuration for dev / prod and watch enabled / disabled
const clientBase = {
  // entry point of the application
  entry: './app/client/app.jsx',

  // output file to the assets folder
  output: {
    path: path.join(__dirname, './app/client/assets/'),
    filename: 'js/bundle.min.js'
  },

  // resolve node_modules imports
  resolve: {
    modules: [
      './node_modules/',
    ],

    extensions: [
      '.js',
      '.json',
      // add .jsx extensions to defaults otherwise eslint is not pleased
      '.jsx',
    ],
  },
};

// client dev build configuration
const clientDev = merge({}, clientBase, {
  // use loaders in dev mode
  module: {
    rules: [
      loaders.js,
      loaders.css(true),
      loaders.sass(true),
      loaders.sassModule(true),
      loaders.variablesSass,
      loaders.url
    ]
  },

  plugins: [
    // extract css from bundle to css file
    extractCss,

    // define NODE_ENV env var because process.env is undefined inside electron renderer process
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"development"'
    }),

    // load loaders in debug mode
    new webpack.LoaderOptionsPlugin({
      debug: true
    })
  ]
});

// client build dev and watch
const clientDevWatch = merge({}, clientDev, {
  // watch for changes
  watch: true,

  // do not watch files in node_modules
  watchOptions: {
    ignored: /node_modules/
  }
});

// build client for production
const clientDist = merge({}, clientBase, {
  cache: false,

  // use loaders in prod mode
  module: {
    rules: [
      loaders.js,
      loaders.css(false),
      loaders.sass(false),
      loaders.sassModule(false),
      loaders.variablesSass,
      loaders.url
    ]
  },

  plugins: [
    // extract css from module to css file
    extractCss,

    // uglify the bundle
    new UglifyJSPlugin({
      test: /\.jsx?$/i
    }),

    // set the NODE_ENV env var since process.env is undefined in renderer process.
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"production"'
    })
  ]
});

module.exports = ({ env = 'dev', watch = 'true' }) => {
  if (env === 'dev') {
    return watch === 'true' ? clientDevWatch : clientDev;
  } else {
    return clientDist;
  }
};
