# Game of Life

For full experience, get a computer with a webcam.

## Start the app

```sh
$ npm install
$ npm run _build:client:prod
$ npm start
```

## Run tests

Tests are written using Facebook's [Jest test framework](https://facebook.github.io/jest/).

```sh
$ npm test
```

## Run linter

The linter used is ESLint with the [airbnb config](https://github.com/airbnb/javascript).

```sh
$ npm run lint
```

## Architecture and technologies

The app is developped using Electron, ReactJS and ReduxJS.

Electron allows us to build a native application using web technologies.

ReactJS is a JS library allowing us to build an interactive application "easily".

ReduxJS is an architecture based on Flux, which makes it easier to reason inside a React application by keeping a single loop of updates of the state of the app.

## Credits

Camera effect was inspired by [https://github.com/idevelop/ascii-camera](https://github.com/idevelop/ascii-camera), who was using [camera.js](https://github.com/idevelop/camera.js).
